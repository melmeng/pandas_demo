import csv
import os
import datetime
import logging
import pandas as pd
import matplotlib.pyplot as plt
from matplotlib.dates import DayLocator, HourLocator, DateFormatter, drange, num2date
from matplotlib.ticker import FuncFormatter
import numpy as np
import pylab
from pandas.tools.plotting import scatter_matrix
logger = logging.getLogger()
FORMAT = '[%(levelname)s]%(filename)s %(lineno)-8s %(message)s'
logging.basicConfig(format=FORMAT)
logger.setLevel(logging.DEBUG)


def diurnal_pattern(fpath, start, end, meters, days, std_l, std_h):

    df = pd.read_csv(fpath, parse_dates=True, index_col=0)
    df = df[start:end]
    
    #get the two columns
    df = df.loc[:, meters]
    cols = meters
    #add week, day, hour of the day    
    df['week'] = [t.isocalendar()[1] for t in df.index]
    df['day'] = [t.isocalendar()[2] for t in df.index]
    df['dhr'] = [t.hour + t.minute/60.0 + t.second/3600.0 for t in df.index]
    
    #filter by days
    def group_by_days(x):
        if x['day'] in days:
            return 'selected'
        else:
            return 'not selected'
    
    df['_day_select'] = df.apply(group_by_days, axis=1)
    
    
    fig, axes = plt.subplots(nrows=len(cols), ncols=1,figsize=(17, 11))
    
    i = 0
    
    
    for col in cols:
        ax = axes[i]
        i+=1
    

        d = df[df['_day_select']=='selected']
        groups = d.groupby('dhr').groups
        
        ax.plot(d['dhr'], d[col], '.', alpha=0.5)
        
        d_filter = None
        for hr in groups.keys():
            idx = groups[hr]
            d3 = d.loc[idx]
            avg = d3.mean()[col]
            std = d3.std()[col]
            d4 = d3[(d3[col]>(avg - std_l*std)) & (d3[col]<(avg + std_h*std))]
            if d_filter is None:
                d_filter = d4
            else:
                d_filter= d_filter.append(d4)
        
        
        ax.plot(d_filter['dhr'], d_filter[col], 'r.', alpha=0.5, label='1 std filter(%s)' % len(d_filter.index))
        d_filter_line = d_filter.groupby('dhr').mean()
        
        ax.plot(d_filter_line.index, d_filter_line[col], 'g-', lw=4, label='mean 1 std filter')
       
        ax.set_title('%s: days(%s)' % (col,','.join([str(x) for x in days])))
        ax.grid(True)
        ax.legend(loc='best')
    plt.show()
    fpath = './workspace/diurnal_pattern/%s_%s_%s.png' % ('-'.join([str(x) for x in days]), start.replace('/', '.'), start.replace('/', '.'))
    fig.savefig(fpath, dpi=75, orientation='landscape')
    logging.info('figure saved: %s' % fpath)
    plt.clf()
    pylab.close()
    f = './workspace/diurnal_pattern/%s_%s_%s.csv' % ('-'.join([str(x) for x in days]), start.replace('/', '.'), start.replace('/', '.'))
    d_filter_line.to_csv(f)
    logging.info('pattern saved: %s' % f)



if __name__=='__main__':
    #path to the observed flow file
    fpath = './workspace/flow/Observed Flow Event.csv'   
    days = [1,2,3,4,5]  #monday is 1, Sunday is 7
    #days = [6,7] 
    start, end = '3/1/2013', '4/1/2013' #start and end dates
    meters = ['CR_%02d' % x for x in range(1, 8)] #meters CR_01 - CR_08
    std_l = 1 
    std_h = 1
    diurnal_pattern(fpath, start, end, meters, days, std_l, std_h)
