import csv
import os
import datetime
import logging
import pandas as pd
import matplotlib.pyplot as plt
from matplotlib.dates import DayLocator, HourLocator, DateFormatter, drange, num2date
from matplotlib.ticker import FuncFormatter
import numpy as np
import pylab
from pandas.tools.plotting import scatter_matrix
logger = logging.getLogger()
FORMAT = '[%(levelname)s]%(filename)s %(lineno)-8s %(message)s'
logging.basicConfig(format=FORMAT)
logger.setLevel(logging.DEBUG)


def check_data(f, start, end, meters):
    df = pd.read_csv(f, parse_dates=True, index_col=0)
    df = df[start:end]
    #get the meters
    df = df.loc[:, meters]
    df.boxplot()
    plt.savefig('./workspace/check_data/boxplot.png')
    # #scatter matrix
    scatter_matrix(df)#  diagonal='kde',)
    plt.savefig('./workspace/check_data/scatter.png')
    # #plot flow data
    df.plot(subplots=True)
    plt.savefig('./workspace/check_data/flow.png')
    df.hist()
    plt.savefig('./workspace/check_data/hist.png')
    plt.show()





if __name__=='__main__':
    #path to the observed flow file
    fpath = './workspace/flow/Observed Flow Event.csv'   
    start, end = '3/1/2013', '4/1/2013' #start and end dates
    meters = ['CR_%02d' % x for x in range(1, 8)] #meters CR_01 - CR_08

    check_data(fpath, start, end, meters)