import pandas as pd
import matplotlib.pyplot as plt
import datetime
import math
import pylab
import csv
import os
import logging
logger = logging.getLogger()
FORMAT = '[%(levelname)s]%(filename)s %(lineno)-8s %(message)s'
logging.basicConfig(format=FORMAT)
logger.setLevel(logging.DEBUG)
logging.info('starting logging')


def read_event(fpath, fmt):
    results = []
    with open(fpath) as f:
        reader = csv.reader(f)
        h = reader.next()
        events = [x for x in h[1:] if x]
        l = reader.next()
        start_dt = dict(zip(h, l))
        for l in reader:
            r = dict(zip(h, l))
            fm = r['meter']
            for event in events:
                if event:
                    dur = r[event]
                    start = datetime.datetime.strptime(start_dt[event], fmt)
                    if dur:
                        dur = float(dur)
                        end = start + datetime.timedelta(seconds=60*dur*60)
                        results.append([fm, start.strftime(fmt), end.strftime(fmt)])

    return results



def plot_all(event_csv, depth_csv, velocity_csv, output_folder, figsize=(4,4)):
    fmt = '%m/%d/%Y %H:%M'
    events = read_event(event_csv, fmt)
    velocity = pd.read_csv(velocity_csv, index_col=0, parse_dates=True)
    depth = pd.read_csv(depth_csv, index_col=0, parse_dates=True)
    
    fms = {}
    for fm, start, end in events:
        fms.setdefault(fm, [])
        fms[fm].append([start, end])
    
    for fm in fms:
        
        fig = plt.figure(figsize=figsize)
        ax = fig.add_subplot(111)
        #plot everything on the background
        x = depth[fm]
        y = velocity.loc[x.index][fm]
        ax.plot(x, y, '.', label='All(%d)' % len(x.index), alpha=0.2)
        ymax = 0
        for start, end in fms[fm]:
            x = depth[fm][start:end]
            y = velocity.loc[x.index][fm]
            ymax = max(ymax, y.max())
            ax.plot(x, y, '.', label='%s(%d)' % (start.split(' ')[0], len(x.index)), alpha=0.5)
        ax.set_xlabel('Depth (ft)')
        ax.set_ylim((0, 1.1*ymax))
        ax.set_ylabel('Velocity(fps)')
        ax.legend(loc='best', fontsize='small', numpoints=1)
        ax.grid(True)
        ax.set_title(fm)
        
        plt.tight_layout()
        fig_path = os.path.join(output_folder, '%s.png' % fm)
        fig.savefig(fig_path, dpi=150, orientation='landscape')
        logging.info('figure saved: %s' % fig_path)
        plt.clf()
        pylab.close()
        
        
        

def scatter_plot(f, index_col=0, fm_col='FM_ID', depth_col='Level_Inches', velocity_col='Velocity_fps', fig_path=''):
    df = pd.read_csv(f, parse_dates=True, index_col=index_col)
    
    start = min(df.index)
    end = max(df.index)
    
    logging.info('Date Range:%s->%s' % (start, end))
    
    fms = df.groupby(fm_col).groups.keys()
    r_ct = math.ceil(len(fms)**0.5)
    logging.info('%d flow meters. Grid size %s X %s' % (len(fms), r_ct, r_ct))
    figsize = (r_ct*4, r_ct*4)
    i = 0
    fig = plt.figure(figsize=figsize)

    
    for fm in sorted(fms):
        i+=1
        t = start
        ax = fig.add_subplot(r_ct, r_ct, i)
        ax.set_title(fm)
        logging.info('Process meter:%s' % fm)
        #monthly plots
        while t<= end:
            start_month = datetime.datetime(t.year, t.month, 1)
            t2 = start_month + datetime.timedelta(seconds=32*24*60*60)
            end_month = datetime.datetime(t2.year, t2.month, 1)
            t = end_month
            
            data = df[df[fm_col]==fm]
            #seems that duplicates were found in the data, so I have to relax the rules.
            idx = [x for x in data.index if x<end_month and x>=start_month]
            data = data.loc[idx]
            logging.info('Filter data between: %s, %s' % (start_month.strftime('%m/%d/%Y'), end_month.strftime('%m/%d/%Y')))
            
            
            x = data[depth_col]
            y = data[velocity_col]
            ax.plot(x, y, '.', label='%s(%d)' % (start_month.strftime('%m/%Y'), len(x)), alpha=0.2)
            ax.set_xlabel('Depth (in)')
            ax.set_ylabel('Velocity(fps)')
            ax.legend(loc='best', fontsize='small', numpoints=1)
            
            ax.grid(True)
        
    
    plt.tight_layout()
    fig.savefig(fig_path, dpi=150, orientation='landscape')
    logging.info('figure saved: %s' % fig_path)
    plt.clf()
    pylab.close()
        
if __name__=='__main__':
    WS = './workspace'
    event_csv = os.path.join(WS, 'flow/events-wwf.csv')
    depth_csv = os.path.join(WS, 'flow/Observed Depth Event.csv')
    velocity_csv = os.path.join(WS, 'flow/Observed Velocity Event.csv')
    output_folder = os.path.join(WS, 'flow_scatter')
    figsize = (4, 4)
    plot_all(event_csv, depth_csv, velocity_csv, output_folder, figsize)